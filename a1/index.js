
const express = require("express");

const app = express();

const port = 3000;

let users = [];

app.use(express.json());

app.use(express.urlencoded({extended: true}));

// GET Route to access /home
app.get("/home",( request, response) => {
		response.send("Welcome to the home page");
})

// POST Route 
app.post("/signUp", (request, response) => {

	if (request.body.username !== '' && request.body.password !== '') {
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
		console.log(request.body);
	} else {
		response.send("Please input BOTH username and password");
	}

})

// GET Route to access /users
app.get("/users",(request, response) => {
	response.send(users);
})


// DELETE 
app.delete("/delete-users", (request, response) => {
	users.splice(users[i], 1);
	response.send(`User ${request.body.username} has been deleted.`);
		
})

app.listen(port, () => console.log(`Server running at port ${port}`));